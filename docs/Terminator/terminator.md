---
author: Laura Fléron
title: Terminator
tags:
  - Installation de Terminator
---

Terminator est un terminal virtuel qui a la particularité de permettre de partager la fenêtre selon vos envies et ainsi organiser plus simplement vos différentes fenêtres. 

### Installation

```bash
sudo apt update && sudo apt install terminator
```

### Lancement
Lancez l'application via le tableau de bord (Unity) ou via le terminal (toutes versions d'Ubuntu) avec la commande suivante :

```bash
terminator
```

### Navigation

Tout son intérêt vient du fait qu'on peut ouvrir des fenêtres avec les commandes suivantes dans une même console :

|commande|action|
|:-|:-|
|Ctrl-Shift-E|Scinder la fenêtre verticalement.|
|Ctrl-Shift-O|Scinder la fenêtre horizontalement.|
|Ctrl-Shift-N ou Ctrl-Tab|Déplacer le curseur dans la fenêtre suivante.|
|Ctrl-Shift-P ou Ctrl-Shift-Tab|Déplacer le curseur dans la fenêtre précédente.|
|Ctrl-Shift-W`|Fermer la fenêtre courante.|
|Ctrl-Shift-X|Agrandir la fenêtre courante.|
|Ctrl-Shift-Flèche`|Agrandir ou rétrécir la fenêtre courante.|
|Ctrl-Tab`|Changer de "split" de fenêtre.|

[source : https://doc.ubuntu-fr.org/terminator](https://doc.ubuntu-fr.org/terminator)
