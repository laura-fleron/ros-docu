---
author: Laura Fléron
title: Installattion
tags:
  - Installation de ROS
---

ROS2 est un framework intéressant à abordé lorsqu’on travaille sur des systèmes embarqués comme les robots.

!!! tip "Le but"

    Dans ce tutoriel, nous allons voir comment installer ROS2 sur une machine Linux et notamment un Raspberry Pi 4 avec une distribution Ubuntu.

    Dans ce dépôt, j'emploie le terme *ROS* pour faire référence à la **version 2** soit ***ROS2***.

Allez c'est parti...

## Téléchargement Ubuntu et configuration de la carte SD

Nous allons configurer la carte SD pour installer ROS2 (Iron Irwini) sur le Raspberry Pi.

Cette version de ROS2 est compatible avec Ubuntu Jammy.

1. Pour cela télécharger l’image de l’OS sous Raspberry Pi Imager > General OS > Ubuntu > Ubuntu Desktop 22.04.3 LTS (64bits)

2. Installer l’image de l’OS à l’aide de Raspberry Pi Imager sur la carte SD. Suivre les instructions de configuration sur l’écran du Raspberry Pi comme pour toutes intallation classique.

??? question "Configurer la connexion à distance"

    Vous pouvez, si vous le désirez, configurer la connexion à distance sur Raspberry Pi. Cette étape est optionnelle mais peut vous simplifier la vie lorsque vous développerez sur ROS2 sous Raspberry Pi.

    N.B.: sur Ubuntu, pour vous connecter à distance sans moniteur, vous devez modifier le fichier /boot/firmware/config.txt

    ```bash
    #dtoverlay=vc4-kms-v3d
    hdmi_force_hotplug=1
    hdmi_group=1
    hdmi_mode=16
    ```

## Installation de ROS2

Pour l’installation de ROS2, vous devez tout d’abord vérifier que le téléchargement de dépôt Universe est activé.

Dans Software & Updates > Ubuntu Software voir :

![les services](SoftwareSources.png)

1. Ajouter ensuite la clé GPG pour ROS2

```bash
sudo apt install curl -y
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg
```

2. Puis ajouter le dépôt à la liste des sources (/etc/source.list.d)

```bash
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null
```

3. Pour le développement, installer les outils de dev

```bash
sudo apt update && sudo apt install ros-dev-tools
```

4. Vous pouvez installer ROS2 en fonction de la version choisi (desktop ou base)
```bash
sudo apt install ros-iron-desktop
```
ou
```bash
sudo apt install ros-iron-ros-base
```

## Pour désinstaller ROS2

1. Pour la désinstallation, il faut supprimer le paquet ainsi que le dépôt

```bash
sudo apt remove ~nros-iron-* && sudo apt autoremove
sudo rm /etc/apt/sources.list.d/ros2.list
sudo apt update
```


