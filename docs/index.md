---
author: Laura Fléron
title: 🏡 Accueil ROS
---

Statut du repo : En tavaux ...

Note de l'autrice

!!! info "But de ce dépôt"

    Ce dépôt est consacré à la découverte de ROS2, avec :

    - L'instalation de ROS2 version *IRON* sur un Raspberry 4,
    - Les concepts du Framework
    - La programmation en Python
    - Le pilotage d'un robot de type Akerman avec codeur rotatif et servo bus.
    

Le Robot Operating System (ROS) est un Framework proposant des librairies ainsi aue des outils pour la construction d'applications en robotique.

ROS est un Framwork open source utile pour tous projets robotiques.

Laura Fléron