---
author: Laura Fléron
title: Les premiers pas avec ROS
tags:
  - Premier pas
---

### Une fois ROS2 installé, vous pouvez charger l’environnement avec

```Bash
source /opt/ros/iron/setup.bash
```

### Vous pouvez tester l’installation à l’aide de l’exemple talker/listener

1. Dans un terminal, lancer le talker :

```bash
source /opt/ros/iron/setup.bash
ros2 run demo_nodes_cpp talker
```

2. Dans un second terminal lancer le listener :

```bash
source /opt/ros/iron/setup.bash
ros2 run demo_nodes_py listener
```
Vous devriez voir dans la console l'échange entre deux nodes :

![listener](ubuntu-ros2-talker-listener.png)

!!! warning "pensez au fichier .bshrc !"

    Nous allons ouvrir plusieurs console et devoir lancr toujours la même source.

    Vous pouvez ajouter cette source au fichier `.bashrc` pour éviter de la lancer après chaque ouverture d'une console.

    ```bash
    echo "source /opt/ros/iron/setup.bash" >> ~/.bashrc
    ```

    Pour supprimer ce lancement automatique, il faut supprimer la ligne dans le fichier `.bashrc` se trouvant ici `/.bashrc` en utilisation un editeur comme vim, nano...

!!! warning "Terminator, la multi console bien utile !"

    Terminator vous permet à partir d'une seule console, l'ouverture de plusieurs fenêtres.

    Un tuto d'installation est ici : [Terminator](# Cest ici)

Pour vérifier la liste des variables d’environnement, faites :

```bash
printenv | grep -i ROS
```
Pour avoir la liste des objets en exécution, faites :

```bash
ros2 node list
ros2 topic list
ros2 service list
ros2 action list
```
