---
author: Laura Fléron
title: TurleSim est ton ami
tags:
  - TurtleSim
---

## Installation de TurtleSim

Un bel outil pour apprendre et bien comprendre le fonctionnement de ROS2 est TurtleSim

Vérifier son installation à l’aide de la commande

```Bash
ros2 pkg executables turtlesim
```

Si ce n’est pas le cas, vous pouvez installer turtlesim à l’aide de la commande :

```bash
sudo apt install ros-iron-turtlesim
```

Dans un terminal lancer turtle sim. Utiliser Terminator serait une bonne idée...

```bash
ros2 run turtlesim turtlesim_node
```
Dans une autre terminal, lancer le contrôle par clavier

```bash
ros2 run turtlesim turtle_teleop_key
```
Nous reviendrons sur ce concept prochainement

Pour observer ce qu’il se passe, dans un troisième terminal, explorer le topic avec la commande suivante :

```bash
ros2 topic echo /turtle1/pose
```

Vous devriez voir dans autre console une tortue se déplaçant losrque vous agissez sur les touches de déplacement dans la console **teleop_key**

![listener](ubuntu-ros2-talker-listener.png)

*[source https://www.aranacorp.com/fr/installer-ros2-sur-raspberry-pi/](https://www.aranacorp.com/fr/installer-ros2-sur-raspberry-pi/)*