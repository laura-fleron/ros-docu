---
author: Laura Fléron
title: L'outil rqt
tags:
  - rqt
---
rqt est une interface graphique permettant de voir les liens entre les nodes et les services, topics.
Permet de paramètrer les nodes et pleins d'autres choses.

## Installation de rqt

Ouvrez un nouveau terminal pour installer *rqt* et ses plugins :

```bash
sudo apt update
sudo apt install '~nros-iron-rqt*'
```
## Executez rqt

```bash
rqt
```
Lorsque vous exécutez rqt pour la première fois, la fenêtre sera vide. Sélectionnez simplement Plugins > Services > Service Caller dans la barre de menu en haut.

!!! tip "Note"

    Il faudra peut-être un certain temps à rqt pour localiser tous les plugins. Si vous cliquez sur Plugins mais que vous ne voyez pas Services ou d'autres options, vous devez fermer rqt et saisir la commande rqt --force-discoverdans votre terminal.

![rqt](rqt.png)

Utilisez le bouton d'actualisation à gauche de la liste déroulante `Service` pour vous assurer que tous les services de votre nœud turtlesim sont disponibles.

Cliquez sur la liste déroulante Service pour voir les services de turtlesim et sélectionnez le service `/spawn`.

### Essayez le service */spawn*

Utilisons rqt pour appeler le service `/spawn`. Nous allons créer une autre tortue dans la fenêtre turtlesim.

Donnez à la nouvelle tortue un nom unique, comme *turtle2*, en double-cliquant entre les guillemets simples vides dans la colonne Expression.

Entrez ensuite des coordonnées valides auxquelles engendrer la nouvelle tortue, comme x = 1.0 et y = 1.0.

![rqt](spawn.png)

!!! note "Note"

    Si vous essayez de faire apparaître une nouvelle tortue portant le même nom qu'une tortue existante, comme celle par défaut turtle1, vous obtiendrez un message d'erreur dans le terminal en cours d'exécutionturtlesim_node :
    
    ```bash
    [ERROR] [turtlesim]: A turtle named [turtle1] already exists
    ```

Pour apparaître turtle2, vous devez ensuite appeler le service en cliquant sur le bouton *Appeler* en haut à droite de la fenêtre rqt.

Si l'appel de service a réussi, vous devriez voir une nouvelle tortue (toujours avec un design aléatoire) apparaître aux coordonnées que vous avez saisies pour x et y .

Si vous actualisez la liste des services dans rqt, vous verrez également qu'il existe désormais des services liés à la nouvelle tortue, *turtle2*, en plus de *turtle1*.

### le service set_pen

Donnons maintenant à turtle1 un stylo unique en utilisant le topic /set_pen :

![rqt](set_pen.png)

- Les valeurs de r , g et b sont comprises entre 0 et 255 et définissent la couleur du stylo turtle1utilisé pour dessiner.
- la largeur définit l'épaisseur de la ligne.

Pour avoir *turtle1* un dessin avec une ligne rouge distincte, changez la valeur de r à 255 et la valeur de largeur à 5.

N'oubliez pas d'appeler le service (topic) après avoir mis à jour les valeurs !

Revenez au terminal où *turtle_teleop_key* est exécuté et appuyez sur les touches fléchées, vous verrez que le stylo de *turtle1* a changé.

Vous avez aussi remarqué qu'il n'y a aucun moyen de déplacer turtle2. C'est parce qu'il n'y a pas de nœud de téléopération pour turtle2. Pour cela nous allons devoir remapper les noeuds...

### Remappage
Vous avez besoin d'un deuxième nœud de téléopération pour contrôler turtle2. Cependant, si vous essayez d'exécuter la même commande que précédemment, vous remarquerez que celle-ci contrôle également turtle1. La façon de modifier ce comportement est de remapper le topic *cmd_vel*.

Dans un nouveau terminal, sourcez ROS 2 et exécutez :

```bash
ros2 run turtlesim turtle_teleop_key --ros-args --remap turtle1/cmd_vel:=turtle2/cmd_vel
```

Désormais, vous pouvez déplacer **turtle2** lorsque ce terminal est actif et **turtle1** lorsque l'autre terminal *urtle_teleop_key* est en cours d'exécution.

### Fermer la simulation

Pour arrêter la simulation, vous pouvez entrer `Ctrl + C` dans la console turtlesim_node, et dans la console *turtle_teleop_key*.

