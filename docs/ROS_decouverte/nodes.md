---
author: Laura Fléron
title: Le concept des Noeuds avec ROS

tags:
  - les noeuds
---

### Comprendre les nœuds

!!! definition "définition"

    Chaque nœud dans ROS doit être responsable d'un seul objectif modulaire comme :
    
    - contrôler les moteurs des roues
    - publier les données d'un capteur de télémètre laser.
    
    Chaque nœud peut envoyer et recevoir des données d'autres nœuds via des topics, services, actions ou des paramètres.

    les topics, services, actions ou des paramètres seront vus dans la suite.

Les nœuds sont la plus petite unité de traitement de ROS 2.

Dans un système ROS 2 typique, de nombreux nœuds sont lancés et communiquent entre eux pour effectuer une tâche complète.

![Fonctionnement](Nodes-TopicandService.gif)

En concevant le système de cette manière modulaire, vous vous assurez que les composants du système sont faiblement couplés et réutilisables dans différents contextes.

Exemple : Si vous avez un robot équipé d'un télémètre laser et de roues motrices, il est préférable d'avoir un nœud qui s'occupe uniquement des capteurs, et un autre nœud dédié au contrôle des moteurs.

La commande ROS2 lance un exécutable issu d'un package.

```bash
ros2 run <package_name> <executable_name>
```

Exemple : pour ouvrir `turtulesim` faire :

```bash
ros2 run turtlesim turtlesim_node
```

Mais à ce stade, nous ne connaissons toujours pas le nom du nœud. Vous pouvez trouver les noms des nœuds en utilisant la commande ros2 node list.

### ros2 node list

La commande `ros2 node list` vous montrera les noms de tous les nœuds **en cours d'exécution**. Cela est particulièrement utile lorsque vous souhaitez interagir avec un nœud ou lorsque vous avez un système avec de nombreux nœuds et que vous devez les suivre.

Ouvrez un nouveau terminal pendant que turtlesim est toujours en cours d'exécution dans un autre terminal, et entrez la commande suivante :

```bash
ros2 node list
```

Le terminal vous renverra le nom du nœud :

```bash
/turtlesim
```

Ouvrez un autre nouveau terminal et démarrez le nœud de téléopération avec la commande :

```bash
ros2 run turtlesim turtle_teleop_key
```

Ici, nous faisons de nouveau référence au package turtlesim, mais cette fois, nous ciblons l'exécutable nommé turtle_teleop_key.

Revenez au terminal où vous avez exécuté ros2 node list et lancez à nouveau la commande. Vous verrez maintenant les noms de deux nœuds actifs :

```bash
/turtlesim
/teleop_turtle
```

### ros2 node info

Maintenant que vous connaissez les noms de vos nœuds, vous pouvez accéder à plus d'informations à leur sujet avec :

```bash
ros2 node info <nom_du_nœud>
```

Pour examiner votre dernier nœud, my_turtle, exécutez la commande suivante :

```bash
ros2 node info /my_turtle
```

La commande ros2 node info renvoie une liste des abonnés, des éditeurs, des services et des actions, c'est-à-dire les connexions du graphe ROS qui interagissent avec ce nœud.

La sortie devrait ressembler à ceci :

```bash
/my_turtle
  Subscribers:
    /parameter_events: rcl_interfaces/msg/ParameterEvent
    /turtle1/cmd_vel: geometry_msgs/msg/Twist
  Publishers:
    /parameter_events: rcl_interfaces/msg/ParameterEvent
    /rosout: rcl_interfaces/msg/Log
    /turtle1/color_sensor: turtlesim/msg/Color
    /turtle1/pose: turtlesim/msg/Pose
  Service Servers:
    /clear: std_srvs/srv/Empty
    /kill: turtlesim/srv/Kill
    /my_turtle/describe_parameters: rcl_interfaces/srv/DescribeParameters
    /my_turtle/get_parameter_types: rcl_interfaces/srv/GetParameterTypes
    /my_turtle/get_parameters: rcl_interfaces/srv/GetParameters
    /my_turtle/list_parameters: rcl_interfaces/srv/ListParameters
    /my_turtle/set_parameters: rcl_interfaces/srv/SetParameters
    /my_turtle/set_parameters_atomically: rcl_interfaces/srv/SetParametersAtomically
    /reset: std_srvs/srv/Empty
    /spawn: turtlesim/srv/Spawn
    /turtle1/set_pen: turtlesim/srv/SetPen
    /turtle1/teleport_absolute: turtlesim/srv/TeleportAbsolute
    /turtle1/teleport_relative: turtlesim/srv/TeleportRelative
  Service Clients:

  Action Servers:
    /turtle1/rotate_absolute: turtlesim/action/RotateAbsolute
  Action Clients:
```

Essayez maintenant d'exécuter la même commande sur le nœud /teleop_turtle et observez comment ses connexions diffèrent de celles de my_turtle.

### Résumé

Un nœud est un élément fondamental de ROS 2 qui remplit un objectif modulaire unique dans un système robotique.

ROS 2 propose plusieurs outils pour examiner et interagir avec les nœuds en cours d'exécution :

> **ros2 node list** : Liste tous les nœuds en cours d'exécution sur le réseau ROS 2.

> **ros2 node info** : Fournit des informations détaillées sur un nœud spécifique, y compris les topics auxquels il est abonné et ceux qu'il publie, ainsi que les services et actions disponibles.
